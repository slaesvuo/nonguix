;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2022 dan <i@dan.games>
;;; Copyright © 2022 Jonathan Brielmaier <jonathan.brielmaier@web.de>
;;; Copyright © 2023 Saku Laesvuori <saku@laesvuori.fi>

(define-module (nongnu packages version-control)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages version-control)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix licenses)
  #:use-module (guix packages)
  #:use-module (ice-9 match)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses))

(define-public helix-core
  (package
    (name "helix-core")
    (version "r22.1")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://ftp.perforce.com/perforce/" version
                                  "/bin.linux26"
                                  (match (%current-system)
                                    ("i686-linux" "x86")
                                    (_ "x86_64"))
                                  "/helix-core-server.tgz"))
              (sha256
               (base32
                (match (%current-system)
                  ("i686-linux"
                   "0f5qs55rspw86axnmml3nxx551lwbxwz1cgi9kmy2f9g5rrplnkn")
                  (_
                   "077rfbjgyhdgv76i2727s3yk3p52y75nml8n9wv8g7mvhfs9ypa9"))))))
    (build-system binary-build-system)
    (arguments
     `(#:strip-binaries? #f
       #:phases (modify-phases %standard-phases
                  (add-before 'patchelf 'patchelf-writable
                    (lambda _
                      (for-each make-file-writable
                                (find-files ".")))))
       #:patchelf-plan '(("p4" ("glibc"))
                         ("p4d" ("glibc"))
                         ("p4p" ("glibc"))
                         ("p4broker" ("glibc")))
       #:install-plan '(("p4" "bin/")
                        ("p4d" "bin/")
                        ("p4p" "bin/")
                        ("p4broker" "bin/"))))
    (inputs (list glibc))
    (home-page "https://www.perforce.com/products/helix-core")
    (synopsis
     "A version control software for large scale development environments")
    (description
     "Helix Core is a version control software for large scale development
environments. The Helix Version Control System manages a central database and
a master repository of file versions.")
    (supported-systems '("i686-linux" "x86_64-linux"))
    (license (nonfree
              "https://www.perforce.com/sites/default/files/pdfs/Helix_Core%20On%20Prem%20Software%20License%20Agmt%20ClickThru_FINAL%2006.28.2021.pdf"))))

(define-public gitea
  (package
    (name "gitea")
    (version "1.20.6")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://github.com/go-gitea/gitea/releases/download/v"
                    version "/gitea-src-" version ".tar.gz"))
              (sha256
               (base32
                "0m3aarf65dcgl2bb8jk2q1f3bhpa00vgi93qgl5zhy8xc12w67i8"))))
    (build-system gnu-build-system)
    (arguments
      (list #:phases
            #~(modify-phases %standard-phases
                (delete 'configure)
                (replace 'build
                  (lambda _
                    (setenv "HOME" (getcwd))
                    (setenv "TAGS" "bindata sqlite sqlite_unlock_notify")
                    (setenv "LDFLAGS"
                      (let* ((format-flag (lambda (var val)
                              (string-append
                                "-X \"code.gitea.io/gitea/modules/setting."
                                var "=" val "\"")))
                             (format-flags (lambda (lst)
                                             (string-join
                                              (map (lambda (x)
                                                     (apply format-flag x))
                                                   lst)))))
                        (format-flags
                          '(("CustomPath" "/var/lib/gitea/custom")
                            ("AppWorkPath" "/var/lib/gitea")))))
                    (invoke "make" "build")))
                (delete 'check)
                (replace 'install
                  (lambda _
                    (install-file "gitea" (string-append #$output "/bin/"))))
                (add-after 'install 'wrap-programs
                  (lambda* _
                    (wrap-program
                     (string-append #$output "/bin/gitea")
                     `("PATH" prefix
                       (,(string-append #$(this-package-input "git")
                                        "/bin")))))))))
    (native-inputs (list go-1.20))
    (inputs (list git bash-minimal))
    (home-page "https://about.gitea.com/")
    (synopsis "Painless self-hosted Git service")
    (description "Gitea is a community managed lightweight code hosting
solution written in Go.  It includes features for Git hosting, code review
and team collaboration.")
    (license expat)))
